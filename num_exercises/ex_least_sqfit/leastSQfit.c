#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

#include"f_header.h"


void leastsqfit(gsl_vector *x,gsl_vector *y,gsl_vector *dy,double (*funs_ptr)(int,double),int m ,int n,gsl_vector *c){

//printf("%lf = atest \n",(*funs_ptr)(1,gsl_vector_get(x,2)));

//---construckt A and b---//

gsl_matrix *A=gsl_matrix_alloc(n,m);
gsl_matrix *R=gsl_matrix_alloc(m,m);
gsl_vector *b=gsl_vector_alloc(n);

for(int i=0;i<n;i++){
for(int k=0;k<m;k++){
double xi = gsl_vector_get(x,i);
double yi = gsl_vector_get(y,i);
double dyi = gsl_vector_get(dy,i);
gsl_matrix_set(A,i,k,(*funs_ptr)(k,xi)/dyi);
gsl_vector_set(b,i,yi/dyi);
}
}

//---------------------//

qr_gs_decomp(A,R);
qr_gs_solve(A, R, b, c);


gsl_matrix_free(R);
gsl_matrix_free(A);
gsl_vector_free(b);
}





double funs(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

int main(){

//aset
gsl_vector *x=gsl_vector_alloc(9);
double xd[]={    0.1f ,   1.33f  ,  2.55f  ,  3.78f   ,    5.0f ,  6.22f  ,  7.45f ,   8.68f  ,   9.9f};
for(int i=0;i<9;i++){gsl_vector_set(x,i,xd[i]);}
gsl_vector *y=gsl_vector_alloc(9);
double yd[]={  -15.3f  ,  0.32f  ,  2.45f   , 2.75f  ,  2.27f  ,  1.35f ,  0.157f ,  -1.23f ,  -2.75f};
for(int i=0;i<9;i++){gsl_vector_set(y,i,yd[i]);}
gsl_vector *dy=gsl_vector_alloc(9);
double dyd[]={  1.04f,   0.594f,   0.983f,   0.998f,    1.11f,   0.398f,   0.535f,   0.968f,   0.478f, };
for(int i=0;i<9;i++){gsl_vector_set(dy,i,dyd[i]);}

gsl_vector *output=gsl_vector_alloc(3);

leastsqfit(x,y,dy,&funs,3,9,output);
double input;
while( scanf("%lg",&input) != EOF ) printf("%lg \t %lg\n",input,funs(0,input)*gsl_vector_get(output,0)+funs(1,input)*gsl_vector_get(output,1)+funs(2,input)*gsl_vector_get(output,2));


gsl_vector_free(x);
gsl_vector_free(y);
gsl_vector_free(dy);
gsl_vector_free(output);


return 0;
}
