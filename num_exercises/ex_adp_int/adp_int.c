#include<math.h>
#include<assert.h>
#include<stdio.h>


double adapt24(double f(double) ,double a ,double b ,double acc ,double eps ,double f2 ,double f3 ,int nrec ){

assert(nrec<1000000 );

double f1=f(a+(b-a)/6) ,   f4=f(a+5*(b-a)/6  ) ;
double Q=(2*f1+f2+f3 +2*f4 )/6.0f*( b-a ) , q=( f1+f4+f2+f3 )/4.0f*( b-a ) ;


double tolerance=acc+eps*fabs(Q);
double error=fabs(Q-q);

if( error<tolerance){
return Q;
}
else{
double Q1=adapt24( f , a , ( a+b )/2.0f , acc/sqrt(2.0f), eps , f1 , f2 , nrec + 1 ) ;
double Q2=adapt24( f , ( a+b )/2.0f , b , acc/sqrt(2.0f), eps , f3 , f4 , nrec + 1 ) ;
return Q1+Q2 ;
}


}

double adapt(double f(double) ,double a ,double b ,double acc ,double eps ){

double f2=f( a +2.0f*(b-a)/6.0f);
double f3=f( a +4.0f*(b-a)/6.0f);

int nrec =0;

return adapt24(f,a,b,acc,eps,f2,f3,nrec) ;

}




int main ( ){

printf("Adaptive integrator example\n");
printf("calculating the folowing functions on [0,1],acc = 1e-5,eps = 1e-5\n");
int int_n = 0;
double a =0.0f;
double b=1.0f ;
double acc = 1e-5;
double eps = 1e-5;

// neasted functions --
double f(double x){int_n ++;return sqrt(x);}
double f1(double x){int_n ++;return 1.0f/sqrt(x);}
double f2(double x){int_n ++;return  log(x)*sqrt(x);}
double f3(double x){int_n ++;return 4*sqrt(1-pow(1-x,2));};
//--------------------
double Q;
Q=adapt(f,a,b,acc,eps);
printf( "sqrt(x) = %g calls = %d \n" ,Q, int_n) ;
Q=adapt(f1,a,b,acc,eps);
printf( "sqrt(x)^(-1) = %g calls = %d \n" ,Q, int_n) ;
Q=adapt(f2,a,b,acc,eps);
printf( "log(x)*sqrt(x) = %g calls = %d \n" ,Q, int_n) ;
Q=adapt(f3,a,b,acc,eps);
printf( "4*sqrt(1-(1-x)²) = %g calls = %d \n" ,Q, int_n) ;
return 0 ;


}
