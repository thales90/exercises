double norm_call(gsl_vector* f,gsl_vector* x,gsl_vector* dx,double lambda){
double rV=0;
int n=x->size;
gsl_vector *temp_x = gsl_vector_alloc(n);

for(int i=0;i<n;i++){
gsl_vector_set(temp_x,i,gsl_vector_get(x,i)+lambda*gsl_vector_get(dx,i));
};
function(temp_x,f);
for(int i=0;i<n;i++){
rV+=pow(gsl_vector_get(f,i),2);
};

gsl_vector_free(temp_x);
return sqrt(rV);
}

void aiteration(gsl_matrix* J, gsl_matrix* R,gsl_vector* x,gsl_vector* f, gsl_vector* dx,int *i_n,double *test_val,int sys_size){
(*i_n)+=1;
printf("\n[ # iteration = %d ]\n",*i_n);

//determin dx;
function(x,f);
Jacobian(x,J);
qr_gs_decomp(J,R);
qr_gs_solve(J,R,f,dx);
//apply line search
double lamb=1.0f;

printf("! lamb call ! : ");
while(norm_call(f,x,dx,lamb)>(1-0.5f*lamb)*norm_call(f,x,dx,0.0f)  && lamb>1.0f/64.0f  ){
lamb=lamb/2.0f;
printf("%lf \t",lamb);
};
printf("\n");
for(int i=0;i<sys_size;i++){
gsl_vector_set(x,i,gsl_vector_get(x,i)+lamb*gsl_vector_get(dx,i));
printf("x[%d] = %lf \t",i,gsl_vector_get(x,i));
}
printf("\n");

*test_val=norm_call(f,x,dx,0);
function(x,f);
//repeate
for(int i=0;i<sys_size;i++){
printf("f[%d] = %lf \t",i,gsl_vector_get(f,i));
}
printf("\ntol = %lf\n\n",*test_val);

}


