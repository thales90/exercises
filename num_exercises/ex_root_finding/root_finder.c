#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

#include"f_header.h"
#include"root_header.h"

//first declear a function R^n -> R^n 

void function(gsl_vector* gsl_x,gsl_vector* gsl_f){
int n=gsl_x->size;
double x[n];
double f[n];
for(int i=0;i<n;i++){x[i]=gsl_vector_get(gsl_x,i);};


// write n number of functions eks n=2;

f[0]=x[0]*x[0];
f[1]=sin(x[1])*x[0];

//


for(int i=0;i<n;i++){gsl_vector_set(gsl_f,i,-1*f[i]);};
}




// declearing jacobian M^n*n

void Jacobian(gsl_vector* gsl_x,gsl_matrix* gsl_J){
int n=gsl_x->size;
double x[n];
double J[n][n];
for(int i=0;i<n;i++){x[i]=gsl_vector_get(gsl_x,i);};

// write n number of functions eks n=2;

J[0][0]=2*x[0];
J[0][1]=0;
J[1][0]=sin(x[1]);
J[1][1]=cos(x[1])*x[0];

//

for(int i=0;i<n;i++){for(int j=0;j<n;j++){gsl_matrix_set(gsl_J,i,j,J[i][j]);};};
}


int main(){

//sys size
int sys_size=2;
double tol = 0.0000001f;
//double lamb=0;
int i_n=0;
gsl_vector *x=gsl_vector_alloc(sys_size);
gsl_vector *dx=gsl_vector_alloc(sys_size);
gsl_vector *f=gsl_vector_alloc(sys_size);
gsl_matrix *J=gsl_matrix_alloc(sys_size,sys_size);
gsl_matrix *R=gsl_matrix_alloc(sys_size,sys_size);

//initial guess 
gsl_vector_set(x,0,4);
gsl_vector_set(x,1,6);

double test_val=100;
while(test_val>tol){

aiteration( J, R, x, f,  dx,&i_n,&test_val,sys_size);

}


gsl_vector_free(x);
gsl_vector_free(dx);
gsl_vector_free(f);
gsl_matrix_free(J);
gsl_matrix_free(R);
return 0;
}
