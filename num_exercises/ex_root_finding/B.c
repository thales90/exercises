#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

#include"f_header.h"

void aiteration(gsl_matrix* J, gsl_matrix* R,gsl_vector* x,gsl_vector* f, gsl_vector* dx,int *i_n,double *test_val,int sys_size);
double norm_call(gsl_vector* f,gsl_vector* x,gsl_vector* dx,double lambda);


//first declear a function R^n -> R^n 

void function(gsl_vector* gsl_x,gsl_vector* gsl_f){
double A = 10000.0f;
int n=gsl_x->size;
double x[n];
double f[n];
for(int i=0;i<n;i++){x[i]=gsl_vector_get(gsl_x,i);};


// write n number of functions eks n=2;

f[0]=1-A*x[0]*x[1];
f[1]=1+1.0f/A-exp(-x[0])-exp(-x[1]);

//


for(int i=0;i<n;i++){gsl_vector_set(gsl_f,i,-1*f[i]);};
}




// declearing jacobian M^n*n

void Jacobian(gsl_vector* gsl_x,gsl_matrix* gsl_J){
double A = 10000.0f;
int n=gsl_x->size;
double x[n];
double J[n][n];
for(int i=0;i<n;i++){x[i]=gsl_vector_get(gsl_x,i);};

// write n number of functions eks n=2;

J[0][0]=-A*x[1];
J[0][1]=-A*x[0];
J[1][0]=exp(-x[0]);
J[1][1]=exp(-x[1]);

//

for(int i=0;i<n;i++){for(int j=0;j<n;j++){gsl_matrix_set(gsl_J,i,j,J[i][j]);};};
}


int main(){
printf("\n\n\n#### testing on demi sys  ####\n\n\n");
//sys size
int sys_size=2;
double tol = 0.0000001f;
//double lamb=0;
int i_n=0;
gsl_vector *x=gsl_vector_alloc(sys_size);
gsl_vector *dx=gsl_vector_alloc(sys_size);
gsl_vector *f=gsl_vector_alloc(sys_size);
gsl_matrix *J=gsl_matrix_alloc(sys_size,sys_size);
gsl_matrix *R=gsl_matrix_alloc(sys_size,sys_size);

//initial guess 
gsl_vector_set(x,0,4);
gsl_vector_set(x,1,6);

double test_val=100;
while(test_val>tol){

aiteration( J, R, x, f,  dx,&i_n,&test_val,sys_size);

}
printf("\n\n\n#### end testing on demi sys  ####\n");
printf("#### ------------------------ ####\n\n\n");

gsl_vector_free(x);
gsl_vector_free(dx);
gsl_vector_free(f);
gsl_matrix_free(J);
gsl_matrix_free(R);
return 0;
}

double norm_call(gsl_vector* f,gsl_vector* x,gsl_vector* dx,double lambda){
double rV=0;
int n=x->size;
gsl_vector *temp_x = gsl_vector_alloc(n);

for(int i=0;i<n;i++){
gsl_vector_set(temp_x,i,gsl_vector_get(x,i)+lambda*gsl_vector_get(dx,i));
};
function(temp_x,f);
for(int i=0;i<n;i++){
rV+=pow(gsl_vector_get(f,i),2);
};

gsl_vector_free(temp_x);
return sqrt(rV);
}

void aiteration(gsl_matrix* J, gsl_matrix* R,gsl_vector* x,gsl_vector* f, gsl_vector* dx,int *i_n,double *test_val,int sys_size){
(*i_n)+=1;
printf("\n[ # iteration = %d ]\n",*i_n);

//determin dx;
function(x,f);
Jacobian(x,J);
qr_gs_decomp(J,R);
qr_gs_solve(J,R,f,dx);
//apply line search
double lamb=1.0f;

printf("! lamb call ! : ");
while(norm_call(f,x,dx,lamb)>(1-0.5f*lamb)*norm_call(f,x,dx,0.0f)  && lamb>1.0f/64.0f  ){
lamb=lamb/2.0f;
printf("%lf \t",lamb);
};
printf(": ! lamb call end! ");
printf("\n");
for(int i=0;i<sys_size;i++){
gsl_vector_set(x,i,gsl_vector_get(x,i)+lamb*gsl_vector_get(dx,i));
printf("x[%d] = %lf \t",i,gsl_vector_get(x,i));
}
printf("\n");

*test_val=norm_call(f,x,dx,0);
function(x,f);
//repeate
for(int i=0;i<sys_size;i++){
printf("f[%d] = %lf \t",i,gsl_vector_get(f,i));
}
printf("\ntol = %lf\n\n",*test_val);

}


