#include<math.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>





void rkstep12( void f(double x ,gsl_vector *yx ,gsl_vector *dydx ) ,int n ,double x , gsl_vector *yx,double h ,gsl_vector *yh ,gsl_vector *dy){
int i ;
gsl_vector *k0=gsl_vector_alloc(n);
gsl_vector *yt=gsl_vector_alloc(n);
gsl_vector *k12=gsl_vector_alloc(n);

f( x,yx , k0 );

for( i=0;i<n;i++) {
gsl_vector_set(yt,i,gsl_vector_get(yx,i)+ gsl_vector_get(k0,i)*h/2.0f);
}
f(x+h/2.0f,yt,k12);
for( i =0; i<n ; i++){
gsl_vector_set(yh,i,gsl_vector_get(yx,i)+ gsl_vector_get(k12,i)*h);
}

for( i =0; i<n ; i++) {
gsl_vector_set(dy,i,(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2.0f);
}
gsl_vector_free(k0);
gsl_vector_free(yt);
gsl_vector_free(k12);
}


int odedriver(void f(double x ,gsl_vector *y ,gsl_vector *dydx ) ,int n ,gsl_vector *xlist, gsl_matrix *ylist, double b ,double h ,double acc,double eps ,int max){
int i, k=0;
double x , s , err , normy , tol , a=gsl_vector_get(xlist,0);
gsl_vector *y=gsl_vector_alloc(n);;
gsl_vector *yh =gsl_vector_alloc(n);
gsl_vector *dy =gsl_vector_alloc(n);



while(gsl_vector_get(xlist,k)<b){

x=gsl_vector_get(xlist,k);
for(int i =0 ;i<n;i++){
gsl_vector_set(y,i,gsl_matrix_get(ylist,k,i));//set to the k row of matrix
}


if(x+h>b){h=b-x;};

rkstep12(f,n,x,y,h,yh,dy);

s=0;
for( i=0; i<n ; i++) {s+=gsl_vector_get(dy,i)*gsl_vector_get(dy,i);}; 
err=sqrt(s);

s=0;
for(i=0;i<n;i++) {s+=gsl_vector_get(yh,i)*gsl_vector_get(yh,i);};
normy=sqrt(s);


tol=(normy*eps+acc)*sqrt(h/(b-a));


if(err<tol){
k++;
if(k>max-1){return k; };
gsl_vector_set(xlist,k,x+h);

for( i =0; i<n ; i++){gsl_matrix_set(ylist,k,i,gsl_vector_get(yh,i));}
}


if(err>0){ h*=pow(tol/err, 0.25f )*0.95f ;}else{h*=2.0f;}

}//end of while

gsl_vector_free(y);
gsl_vector_free(yh);
gsl_vector_free(dy);

return k+1;
}




int main(){

int n=4;
double y0i;
double y1i;
double y2i;
double y3i;
double b;
int max;
printf("Wellcome to this ode Demo !\n");
printf("i have implementet the ruḱstepper of type 12.\n");
printf("to demostrate we solve the simplest 2d centreal spring problem !\n");
printf("saying m k and l equals to 1, and we are working in polar cordinates\n");
printf("intial val sugestion (1,1,1,1,10,100000\n");
printf("2H=(Pr²+Pp²)+(r-1)²\n");
printf("please enter initial value for r:\n");
scanf("%lf",&y0i);
printf("please enter initial value for Pr:\n");
scanf("%lf",&y1i);
printf("please enter initial value for phi:\n");
scanf("%lf",&y2i);
printf("please enter initial value for Pphi:\n");
scanf("%lf",&y3i);
printf("please enter emulation time:\n");
scanf("%lf",&b);
printf("please enter max number step:\n");
scanf("%d",&max);
double a=0;
double h=0.01f;
double acc=0.00001f;
double eps=0.00001f;
double ti =0;






gsl_vector *x=gsl_vector_alloc(max);
gsl_vector_set(x,0,ti);
gsl_matrix *y=gsl_matrix_alloc(max,n);
gsl_matrix_set(y,0,0,y0i);
gsl_matrix_set(y,0,1,y1i);
gsl_matrix_set(y,0,2,y2i);
gsl_matrix_set(y,0,3,y3i);
gsl_vector *dy=gsl_vector_alloc(n);

void f(double x,gsl_vector *y,gsl_vector *dy){

//the equation
double k=1.0f;
double m=1.3f;
double lo=0.7f;
double y0=gsl_vector_get(y,0);
double y1=gsl_vector_get(y,1);
double y2=gsl_vector_get(y,2);
double y3=gsl_vector_get(y,3);
double dy0=y1/m;
double dy1=-k*(y0-lo);
double dy2=y3/m;
double dy3=0;
gsl_vector_set(dy,0,dy0);
gsl_vector_set(dy,1,dy1);
gsl_vector_set(dy,2,dy2);
gsl_vector_set(dy,3,dy3);
}

// this is where we call the driver : 
int nn = odedriver(f,n,x,y,b,h,acc,eps,max);
//after we have a gsl matrix y(max,dimension) containing the solution; 

double r , phi;
FILE *out_put;
out_put=fopen("data_file.txt","w");
for(int i = 0;i<nn;i++){

r=gsl_matrix_get(y,i,0);
phi=gsl_matrix_get(y,i,2);

fprintf(out_put,"%lf \t %lf\n",r*cos(phi),r*sin(phi));
}
return 0;
}








