#include<assert.h>
#include<stdio.h>
#include<math.h>
double linterp(int n, double *x, double *y, double z);





int main(){

int N=33;
int M =200;
double x[N];
double y[N];
for(int i=0 ;i<N;i++){
x[i]=3.2f/(float)(N-1)*i;
y[i]=sin(8*x[i])*exp(-x[i]);
}




for(int j=0;j<M;j++){

double val=linterp(N,x,y,x[N-1]/(float)(M)*j);
printf("%lf \t %lf\n",x[N-1]/(float)(M)*j,val);

}

return 0;
}




double linterp(int n, double *x, double *y, double z){

assert(n>1 && z>=x[0] && z<=x[n-1]);
int i=0,j=n-1;
while(j-i>1){
int m=(i+j)/2;
if(z>x[m]){ i=m; }else{ j=m;}
}

return y[i] +(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);

}





