#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
struct qspline {int n; double *x, *y, *b, *c;};

struct qspline *qspline_alloc(int n, double *x, double *y, double *b, double *c); /* allocates and builds the quadratic spline */
double qspline_evaluate(struct qspline *s, double z);        /* evaluates the prebuilt spline at point z */
void qspline_free(struct qspline *s); /* free memory allocated in qspline_alloc */


int main(){
int N=33;
double x[N];
double y[N];
for(int i=0 ;i<N;i++){
x[i]=3.2f/(float)(N-1)*i;
y[i]=sin(8*x[i])*exp(-x[i]);
}



double b[N];
double c[N];

struct qspline *test =qspline_alloc(N,x,y,b,c);
int M=300;
for(int j=0;j<M;j++){

double val=qspline_evaluate(test,x[N-1]/(float)(M)*j);
printf("%lf \t %lf\n",x[N-1]/(float)(M)*j,val);

}



qspline_free(test);

return 0;
}

struct qspline* qspline_alloc(int n, double *x, double *y,double *b,double *c){

struct qspline *pointer_qspline;
pointer_qspline= malloc(sizeof(struct qspline));
pointer_qspline->n=n;
pointer_qspline->x=x;
pointer_qspline->y=y;
pointer_qspline->b=b;
pointer_qspline->c=c;

int i;
	double p[n-1], h[n-1];                  //VLA from C99
	for(i=0;i<n-1;i++){
		h[i]=x[i+1]-x[i];
		p[i]=(y[i+1]-y[i])/h[i];
	}
	pointer_qspline->c[0]=0;                                     //recursion up:
	for(i=0;i<n-2;i++)
		pointer_qspline->c[i+1]=(p[i+1]-p[i]-pointer_qspline->c[i]*h[i])/h[i+1];
	pointer_qspline->c[n-2]/=2;                                 //recursion down:
	for(i=n-3;i>=0;i--)
		pointer_qspline->c[i]=(p[i+1]-p[i]-pointer_qspline->c[i+1]*h[i+1])/h[i];
	for(i=0;i<n-1;i++)
		pointer_qspline->b[i]=p[i]-pointer_qspline->c[i]*h[i];


return pointer_qspline ;

}

double qspline_evaluate(struct qspline *s, double z){


assert( z>=s->x[0] &&  z<=s->x[s->n-1] );
int i =0,j=s->n-1;
while( j-i>1){
int m=( i+j )/2.0f ;
if ( z>s->x[m] ) {  i=m;}else{ j=m;}}
double h=z-(s->x[i]);
return (s->y[i])+h*( (s->b[i])+h*(s->c[i]) ) ;

}





void qspline_free(struct qspline *s){

free(s);

}
