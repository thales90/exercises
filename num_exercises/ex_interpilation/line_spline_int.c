#include<assert.h>
#include<stdio.h>
#include<math.h>
double linterp_integ(int n, double *x, double *y, double z);



int M =100; 
double z_o=1.82f;

int main(){


for(int j=0;j<M;j++){
int N=10+j; 
double x[N];
double y[N];
for(int i=0;i<N;i++){
x[i]=(2.5f/(float)(N))*i;
y[i]=sin(8*x[i])*exp(-x[i]);} //reproducing data set

double val=linterp_integ(N,x,y,z_o);
printf("%d \t %lf\n",N,val);

}




return 0;
}


double linterp_integ(int n, double *x, double *y, double z){
double ret_val=0;
assert(n>1 && z>=x[0] && z<=x[n-1]);
int i=0,j=n-1;
while(j-i>1){
int m=(i+j)/2;
if(z>x[m]){ i=m; }else{ j=m;}
}
int q;
for ( q =0;q<i;q++){
ret_val+=(y[q+1]-y[q])/(x[q+1]-x[q])*(x[q+1]*x[q+1]*0.5f+x[q]*x[q]*0.5f-x[q]*x[q+1])+y[q]*(x[q+1]-x[q]);
}
ret_val+=(y[i+1]-y[i])/(x[i+1]-x[i])*(z*z*0.5f+x[i]*x[i]*0.5f-x[i]*z)+y[i]*(z-x[i]);

return ret_val;

}



