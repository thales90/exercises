#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

void show_matrix_consol(gsl_matrix* A,int w,int h);
void jacobi_matrix_set(gsl_matrix* Jpq,int n,int p, int q,gsl_matrix* A,double phi);
bool jacobi_sweep(gsl_matrix* A,gsl_matrix* S);
void show_matrix_consol(gsl_matrix* A,int h, int w);


int main(int argc,char *argv[]){

int test_size=atoi(argv[1]);
//alloc :
gsl_matrix *A=gsl_matrix_alloc(test_size,test_size);
gsl_matrix *S=gsl_matrix_alloc(test_size,test_size);
gsl_matrix *Test=gsl_matrix_alloc(test_size,test_size);
//Set sym real mat:
for(int i=0;i<test_size;i++){
for(int j=0;j<test_size;j++){



gsl_matrix_set(A,i,j,(i+1)*(j+1));


if(i==j){gsl_matrix_set(S,i,j,1);
}else{gsl_matrix_set(S,i,j,0);
}

}
}

//Sweeping time;
bool toend=false;
int i_n=0;

while(toend!=true){
i_n++;
toend=jacobi_sweep(A,S);
};


gsl_matrix_free(S);
gsl_matrix_free(A);
gsl_matrix_free(Test);
return 0;

}




double dia_set(gsl_matrix *A,gsl_vector *pd){
double rd=0;
int nn = A->size1; //same;
for(int i=0;i<nn;i++){
rd+=sqrt(pow(gsl_matrix_get(A,i,i)-gsl_vector_get(pd,i),2));
}
return rd;
}




bool jacobi_sweep(gsl_matrix* A,gsl_matrix* S){


bool re_state=false;
double tol =0.0000000001f;
int r = A->size1; //same;
int c = A->size2;

gsl_vector *pd= gsl_vector_alloc(r);
for(int i=0;i<r;i++){
gsl_vector_set(pd,i,gsl_matrix_get(A,i,i));
}


gsl_matrix* cJ=gsl_matrix_alloc(r,r);
gsl_matrix* Holder=gsl_matrix_alloc(r,r);
gsl_matrix* Holder2=gsl_matrix_alloc(r,r);
gsl_matrix* Holder3=gsl_matrix_alloc(r,r);

for(int p=0;p<r;p++){
for(int q=p+1;q<c;q++){
//int p=2,q=1; // test val 
double phi = 0.5f*atan(2.0f*gsl_matrix_get(A,p,q)/(gsl_matrix_get(A,q,q)-gsl_matrix_get(A,p,p)));

jacobi_matrix_set(cJ,r,p, q,A,phi);
//show_matrix_consol(cJ,r,c);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,S,cJ,0.0f,Holder2);
//gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,Holder2,cJ,0.0f,S);
//gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,cJ,Holder2,0.0f,S);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,cJ,A,0.0f,Holder);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,Holder,cJ,0.0f,A);

for(int p=0;p<r;p++){
for(int q=0;q<c;q++){
gsl_matrix_set(S,p,q,gsl_matrix_get(Holder2,p,q));
}
}

//show_matrix_consol(S,r,c);


}
}


gsl_matrix_free(cJ);
gsl_matrix_free(Holder);
gsl_matrix_free(Holder2);
gsl_matrix_free(Holder3);
double test=dia_set(A,pd);
//printf("%.*lf tol \n\n",20,test);


if(test<tol){re_state=true;}

gsl_vector_free(pd);
return re_state;
}







void show_matrix_consol(gsl_matrix* A,int h, int w){

printf("\n\n");
for(int i=0;i<h;i++){
for(int j=0;j<w;j++){
printf("%lf \t ", gsl_matrix_get(A,i,j));
}
printf("\n\n");
}
printf("\n\n");

}







void jacobi_matrix_set(gsl_matrix* Jpq ,int n,int p, int q,gsl_matrix* A,double phi){


for(int i =0;i<n;i++){
for(int j =0;j<n;j++){


if(i==j){
gsl_matrix_set(Jpq,i,j,1);
}else{
gsl_matrix_set(Jpq,i,j,0);
}

if (i==p && j==p){
gsl_matrix_set(Jpq,i,j,cos(phi));
}

if (i==q && j==q){
gsl_matrix_set(Jpq,i,j,cos(phi));
}

if (i==p && j==q){
gsl_matrix_set(Jpq,i,j,sin(phi));
}
if (i==q && j==p){
gsl_matrix_set(Jpq,i,j,-1*sin(phi));
}

}
}




}


