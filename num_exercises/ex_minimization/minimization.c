#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

#include"f_header.h"
void aiteration(gsl_matrix* H, gsl_matrix* R,gsl_vector* x, gsl_vector* df,gsl_vector* dx,int *i_n,double *test_val,int sys_size,double *f);
void function(gsl_vector* gsl_x,gsl_vector* gsl_df ,gsl_matrix* gsl_H ,double *f){
double b = 100.0f;
int n=gsl_x->size;
double x[n];
double df[n];
double H[n][n];
for(int i=0;i<n;i++){x[i]=gsl_vector_get(gsl_x,i);};

*f=(1-x[0]*x[0])*(1-x[0]*x[0])+b*(x[1]-x[0]*x[0])*(x[1]-x[0]*x[0]);
// write n number of functions eks n=2;

df[0]=2*(2*b*x[0]*x[0]*x[0]-2*x[0]*x[1]*b+x[0]-1);
df[1]=2*b*(x[1]-x[0]*x[0]);

//

H[0][0]=12*b*x[0]*x[0]-4*b*x[1]+2;
H[0][1]=-4*b*x[0];
H[1][0]=-4*b*x[0];
H[1][1]=2*b;

//

for(int i=0;i<n;i++){gsl_vector_set(gsl_df,i,-1*df[i]);};
for(int i=0;i<n;i++){for(int j=0;j<n;j++){gsl_matrix_set(gsl_H,i,j,H[i][j]);};};


}

bool armio(gsl_vector *x,gsl_vector *dx,gsl_vector *df,gsl_matrix *H ,double lamb,double *f,int sys_size){
bool r_bool=false;
double alpha =0.00001f;
double px=*f;
double Aa;
gsl_vector* s=gsl_vector_alloc(sys_size);
gsl_vector* cx=gsl_vector_alloc(sys_size);
for(int i=0;i<sys_size;i++){
gsl_vector_set(s,i,lamb*gsl_vector_get(dx,i));
}
for(int i=0;i<sys_size;i++){
gsl_vector_set(cx,i,gsl_vector_get(x,i));
}
gsl_blas_ddot(s,df,&Aa);
Aa=Aa*alpha;

gsl_vector_add(cx,s);
function(cx,df,H,f);
double m=*f;
if (m<Aa+px){
r_bool=true;
}else{
r_bool=false;
}

function(x,df,H,f);

gsl_vector_free(s);
gsl_vector_free(cx);
return r_bool;
}



int main(){


int size_sys=2;
double f=0;
double tol=0.0001f;
gsl_vector *x=gsl_vector_alloc(size_sys);
gsl_vector *dx=gsl_vector_alloc(size_sys);
gsl_vector *df = gsl_vector_alloc(size_sys);
gsl_matrix *H = gsl_matrix_alloc(size_sys,size_sys);
gsl_matrix *R = gsl_matrix_alloc(size_sys,size_sys);
//initialise x 
gsl_vector_set(x,0,4);
gsl_vector_set(x,1,5);

function(x,df,H,&f);

qr_gs_decomp(H,R);
qr_gs_solve(H, R, df,dx);
int i_n=0;
double test_val=100;
while(test_val>tol){
aiteration(H, R,x,df, dx,&i_n,&test_val,size_sys,&f);
}




/*
show_matrix_consol(H,size_sys,size_sys);
for(int i=0;i<size_sys;i++){
printf("df[%d]=%lf\n",i,gsl_vector_get(df,i));
}
*/

gsl_vector_free(x);
gsl_vector_free(dx);
gsl_vector_free(df);
gsl_matrix_free(H);
}


void aiteration(gsl_matrix* H, gsl_matrix* R,gsl_vector* x,gsl_vector* df, gsl_vector* dx,int *i_n,double *test_val,int sys_size,double *f){
(*i_n)+=1;
printf("\n[ # iteration = %d ]\n",*i_n);

//determin dx;
function(x,df,H,f);
qr_gs_decomp(H,R);
qr_gs_solve(H,R,df,dx);
//apply line search armijos condition;


printf("! lamb call ! : ");
double lamb=1.0f;
while(armio(x,dx,df,H,lamb,f,sys_size)!=true&&lamb>1.0f/64.0f){
lamb=lamb/2.0f;
printf("%lf \t",lamb);
}
printf(": ! lamb call end! ");
printf("\n");



for(int i=0;i<sys_size;i++){
gsl_vector_set(x,i,gsl_vector_get(x,i)+lamb*gsl_vector_get(dx,i));
printf("x[%d] = %lf \t",i,gsl_vector_get(x,i));
}


printf("\n");
*test_val=gsl_blas_dnrm2(dx); //abs(*f-t1);

function(x,df,H,f);

//repeate

printf("f= %.*lf \n",20,*f);
printf("\ntol = %.*lf\n\n",20,*test_val);


}





