#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>//for testing

#include"f_header.h"


//int gsl_blas_ddot(const gsl_vector * x, const gsl_vector * y, double * result);


int main(){


gsl_matrix *A=gsl_matrix_alloc(3,3);
gsl_matrix *R=gsl_matrix_alloc(3,3);

gsl_vector *b=gsl_vector_alloc(3);
gsl_vector *x=gsl_vector_alloc(3);

gsl_vector *b_result=gsl_vector_alloc(3);
gsl_vector *x_result=gsl_vector_alloc(3);
gsl_matrix *test_transpose=gsl_matrix_alloc(3,3);
gsl_matrix *test_transpose2=gsl_matrix_alloc(3,3);


gsl_matrix *test_decompose=gsl_matrix_alloc(3,3);

for(int i=0;i<3;i++){
for(int j=0;j<3;j++){
gsl_matrix_set(A,i,j,(float)(rand())/(float)(rand()));
}
}

show_matrix_consol(A,3,3);


printf(" \n\n---- after qr_gs_decomp() ----\n\n ");
qr_gs_decomp(A,R);

printf(" \n\n---- Q ----\n\n ");

show_matrix_consol(A,3, 3);

printf(" \n\n---- R----\n\n ");

show_matrix_consol(R,3, 3);

gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,A,A,0.0f,test_transpose);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,A,R,0.0f,test_decompose);

printf(" \n\n---- Decompose test Q*R  ----\n\n ");
show_matrix_consol(test_decompose,3,3);

printf(" \n\n---- Transpose test Q^T*Q ----\n\n ");
show_matrix_consol(test_transpose,3,3);

printf(" \n\n---- Transpose test Q*Q^T ----\n\n ");

gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0f,A,A,0.0f,test_transpose2);
show_matrix_consol(test_transpose2,3,3);

printf(" \n\n---- Atempet to solve by back pushing  ---- \n\n");

//create random b

for (int p=0 ;p<3;p++){
gsl_vector_set(b,p,(float)(rand())/(float)(rand()));
printf(" b[%d] = %lf \n",p,gsl_vector_get(b,p));
}
qr_gs_solve(A, R, b, x);
printf(" \n\n---- x after solver function  ---- \n\n");

for (int p=0 ;p<3;p++){
printf(" x[%d] = %lf \n",p,gsl_vector_get(x,p));
}

printf(" \n\n---- Q*x ---- \n\n");


gsl_blas_dgemv(CblasNoTrans,1,test_decompose,x,0,b_result);

for (int p=0 ;p<3;p++){
printf(" b_result[%d] = %lf \n",p,gsl_vector_get(b_result,p));
}



gsl_vector_free(b);
gsl_vector_free(x);
gsl_matrix_free(A);
gsl_matrix_free(R);

gsl_vector_free(b_result);
gsl_vector_free(x_result);
gsl_matrix_free(test_decompose);
gsl_matrix_free(test_transpose);
gsl_matrix_free(test_transpose2);





}








