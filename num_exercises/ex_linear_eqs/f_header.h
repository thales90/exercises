#ifndef HAVE_GSL
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#define HAVE_GSL
#endif
void qr_gs_decomp(gsl_matrix* A,gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x);
void show_matrix_consol(gsl_matrix* A,int w,int h);


void qr_gs_decomp(gsl_matrix* A,gsl_matrix* R){


int r=A->size1;
int c=A->size2;


gsl_vector *temp_a=gsl_vector_alloc(r);
gsl_vector *temp_aj=gsl_vector_alloc(r);
gsl_vector *temp_q=gsl_vector_alloc(r);
double norm_res;
double qaj_dot;

//first loop ortogonalisering
for (int q=0 ;q<c;q++){

//ai pick
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_a,p,gsl_matrix_get(A,p,q));
}
//find norm
gsl_blas_ddot(temp_a, temp_a, &norm_res);
norm_res=sqrt(norm_res);
//set Rqq
gsl_matrix_set(R,q,q,norm_res);
//qi pick
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_q,p,gsl_vector_get(temp_a,p)/norm_res);
gsl_matrix_set(A,p,q,gsl_vector_get(temp_a,p)/norm_res);
}
//du har nu qi og ai
for(int j=q+1;j<c;j++){
//set aj
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_aj,p,gsl_matrix_get(A,p,j));
}
//find norm
gsl_blas_ddot(temp_q, temp_aj, &qaj_dot);
// set Rqj
gsl_matrix_set(R,q,j,qaj_dot);
// set Qpj
for (int p=0 ;p<r;p++){
gsl_matrix_set(A,p,j,gsl_vector_get(temp_aj,p)-qaj_dot*gsl_vector_get(temp_q,p));
}

}


}

gsl_vector_free(temp_a);
gsl_vector_free(temp_aj);
gsl_vector_free(temp_q);


}

////solver 

void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R,gsl_vector* b,gsl_vector* x){


int r=Q->size1;
int c=Q->size2;

gsl_vector *temp_c=gsl_vector_alloc(c);
gsl_vector *temp_c2=gsl_vector_alloc(r);

//first create c by mult transpose
gsl_blas_dgemv(CblasTrans,1,Q,b,0,temp_c);
gsl_blas_dgemv(CblasNoTrans,1,Q,temp_c,0,temp_c2);
//brug back substitution på c og R 
for(int i =c-1 ;i>=0;i--){


double cc = gsl_vector_get(temp_c,i);
for(int k=i+1;k<c;k++){

cc-=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
}

gsl_vector_set(x,i,cc/gsl_matrix_get(R,i,i));

}

gsl_vector_free(temp_c);
gsl_vector_free(temp_c2);
}




void show_matrix_consol(gsl_matrix* A,int h, int w){

printf("\n\n");
for(int i=0;i<h;i++){
for(int j=0;j<w;j++){
printf("%lf \t ", gsl_matrix_get(A,i,j));
}
printf("\n\n");
}
printf("\n\n");


}
