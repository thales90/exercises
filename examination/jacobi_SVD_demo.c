#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<math.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

#include<gsl/gsl_blas.h>  // basic operation , up to products; 

#include"jacobi_SVD_algo.h" // header indcluding algoritme.



int c;
int r;
int main(){


printf("\n\n\n");
printf("#####################################\n");
printf("##    Jacobi SVD algoritme Demo    ##\n");
printf("##      Student Thomas Nielsen     ##\n");
printf("##          aka 201406295          ##\n");
printf("##      exersice 95 mod 22 = 7     ##\n");
printf("#####################################\n");
printf("\n\n\n");
printf("Please enter size of matrix : \n\n");

printf("Please enter number of colums m : \n\n");
scanf("%d",&c);
while(c<1){
printf("\n( !error! ) m must be larger the zero pls try agien: \n\n");
scanf("%d",&c);
}

printf("\nPlease enter number of rows n>=m : \n\n");
scanf("%d",&r);
while(c>r){
printf("\n( !error! ) n must be larger or equal to m pls try agien: \n\n");
scanf("%d",&r);
}

printf("\nyou have chosen a matrix of size %d x %d !  \n\n",r,c);

// ----------- Decleare Demo variables -----------

gsl_matrix *A=gsl_matrix_alloc(r,c);
gsl_matrix *U=gsl_matrix_alloc(r,c);
gsl_matrix *V=gsl_matrix_alloc(c,c);
gsl_matrix *D=gsl_matrix_alloc(c,c);

gsl_matrix_set_identity(U);
gsl_matrix_set_identity(V);

// ------------------------------------------------

printf("\ndo you wish to enter your own matrix ? ( enter 1 )\n");
printf("or do you wish to have your matrix filled in random ? ( enter 0 or not 1 ) \n\n");
double temp_h;
scanf("%lf",&temp_h);
if (temp_h==1){
printf("\nYou chose to fill in your own matrix !\n\n");
for(int i=0;i<r;i++){for(int j=0;j<c;j++){printf("please enter a value for M(%d,%d) : \n\n",i,j);scanf("%lf",&temp_h);gsl_matrix_set(A,i,j,temp_h);}}
}else{
printf("\nYou chose to Auto fill your matrix !\n\n");
for(int i=0;i<r;i++){for(int j=0;j<c;j++){gsl_matrix_set(A,i,j,(float)(rand())/(float)(rand()));}}}
printf("Hers YOur Matrix >>'A'<< Lets get to the fun stuff !\n\n");
show_matrix_consol(A);

int i_n;
printf("\n         -------- Calculation commence ! --------\n\n");
i_n=jacobi_SVD_algo(A,U,V,D);
printf("\n\n        ----------   Calculation Endet !  ----------\n\n");

printf("\n\t---------- U -----------\n\n");
show_matrix_consol(U);
printf("\n\t---------- V -----------\n\n");
show_matrix_consol(V);
printf("\n\t---------- D -----------\n\n");
show_matrix_consol(D);

printf("\n\t ---- Finaly Testing if A = UDV^T ---- \n" );
gsl_matrix *TEST = gsl_matrix_alloc(r,c);

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,U,D,0.0f,TEST);
gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0f,TEST,V,0.0f,A);
show_matrix_consol(A);

printf("\n\tNumber of iterations , as in number of sweeps  = %d \n\n",i_n);

gsl_matrix_free(TEST);
printf("\n\n");
printf("#####################################\n");
printf("##           End of Demo           ##\n");
printf("#####################################\n");

// ----------- free Demo variables -----------

gsl_matrix_free(A);
gsl_matrix_free(U);
gsl_matrix_free(D);
gsl_matrix_free(V);

// -------------------------------------------


return 0;
}












