double jacobi_sweep( gsl_matrix *A,gsl_matrix *U,gsl_matrix *V,gsl_matrix *D );
int jacobi_SVD_algo(gsl_matrix *A,gsl_matrix *U,gsl_matrix *V,gsl_matrix *D);
void jacobi_matrix_set(gsl_matrix* Jpq ,int p, int q,gsl_matrix* A,double phi);
void show_matrix_consol(gsl_matrix* A);
double dia_check(gsl_vector* tol_v,gsl_matrix *A);
void qr_gs_decomp(gsl_matrix* A,gsl_matrix* R);

// iterator ----------------------------------------------------------------

int jacobi_SVD_algo(gsl_matrix *A,gsl_matrix *U,gsl_matrix *V,gsl_matrix *D){

// do relocation:
 
int r = A->size1;
int c = A->size2;
int return_int=0;

if (r>c){

printf("\n    ------------------ case of n>m ---------------------\n");
printf(" ------------------ Decomposition A=QR ---------------------\n\n");
gsl_matrix *Q =gsl_matrix_alloc(r,c);
gsl_matrix *R =gsl_matrix_alloc(c,c);
gsl_matrix *Uo =gsl_matrix_alloc(c,c);
gsl_matrix_set_identity(Uo);


gsl_matrix_memcpy(Q,A);
qr_gs_decomp(Q,R);

printf("\n\t---------- Q -----------\n\n");
show_matrix_consol(Q);
printf("\n\t---------- R -----------\n\n");
show_matrix_consol(R);



//      --------------  this bit is only for demo use  ------------ 
//      --------------  Demonstrat the Givens Rotation  -----------

printf("\n------------------ Givens Demonstration ---------------------\n");
printf("--------------------- Givens(1,2) * R  ----------------------\n\n");

int p=0;
int q=1;
gsl_matrix *Holder_J=gsl_matrix_alloc(R->size1,R->size2);
double theta = atan2(-gsl_matrix_get(R,q,p)+gsl_matrix_get(R,p,q),gsl_matrix_get(R,q,q)+gsl_matrix_get(R,p,p));
jacobi_matrix_set( Holder_J ,p,  q , R,theta);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,Holder_J,R,0.0f,D);
show_matrix_consol(D);
gsl_matrix_free(Holder_J);
printf("\n\nOne sees that the Givesrotation has alterede the values of the matrix.\n");
printf("But more importently index R(2,1) and R(1,2) has become equale.\n");
printf("Thus setting up for the implementation of the jacobi transformation.\n\n");
printf("Not after Givens Rotation R is not upper triangular , but this is expected.\n\n");
printf("\n\n---------------- End of Givens Demonstration -----------------\n");
//      -----------------------------------------------------------

return_int=0;
double tol_check_val=10000000;
//main loop
while (tol_check_val>0.0f){
tol_check_val=jacobi_sweep(R,Uo,V,D);
return_int++;
}

//re creating A; This is not very elegant.

gsl_matrix *Holder=gsl_matrix_alloc(A->size1,A->size2);

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,Q,Uo,0.0f,Holder);
gsl_matrix_memcpy(U,Holder);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,Holder,R,0.0f,Q);
gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0f,Q,V,0.0f,A);

gsl_matrix_free(Holder);

gsl_matrix_free(Q);
gsl_matrix_free(R);
gsl_matrix_free(Uo);



}else{





//------

//      --------------  this bit is only for demo use  ------------ 
//      --------------  Demonstrat the Givens Rotation  -----------

printf("\n------------------ Givens Demonstration ---------------------\n");
printf("--------------------- Givens(1,2) * A  ----------------------\n\n");

int p=0;
int q=1;
gsl_matrix *Holder_J=gsl_matrix_alloc(A->size1,A->size2);
double theta = atan2(-gsl_matrix_get(A,q,p)+gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q)+gsl_matrix_get(A,p,p));
jacobi_matrix_set( Holder_J ,p,  q , A,theta);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,Holder_J,A,0.0f,D);
show_matrix_consol(D);
gsl_matrix_free(Holder_J);
printf("\n\nOne sees that the Givesrotation has alterede the values of the matrix.\n");
printf("But more importently index A(2,1) and A(1,2) has become equale.\n");
printf("Thus setting up for the implementation of the jacobi transformation.\n\n");
printf("\n\n---------------- End of Givens Demonstration -----------------\n");
//      -----------------------------------------------------------



return_int=0;
double tol_check_val=10000000;
//main loop
while (tol_check_val>0.0f){
tol_check_val=jacobi_sweep(A,U,V,D);
return_int++;
}

//re creating A; This is not very elegant.
gsl_matrix *Holder=gsl_matrix_alloc(A->size1,A->size2);

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,U,D,0.0f,Holder);
gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0f,Holder,V,0.0f,A);

gsl_matrix_free(Holder);



}


return return_int;// # iterations 






}

// -------------------------------------------------------------------------

// Sweep -------------------------------------------------------------------

double jacobi_sweep( gsl_matrix *A,gsl_matrix *U,gsl_matrix *V,gsl_matrix *D ){

int r =A -> size1;
int c =A -> size2;


gsl_vector *tol_v=gsl_vector_alloc(r); // to check for converge
for(int p=0;p<r;p++){gsl_vector_set(tol_v,p,gsl_matrix_get(A,p,p));} // saving pre-diagonal


gsl_matrix *J=gsl_matrix_alloc(r,c);
gsl_matrix *G=gsl_matrix_alloc(r,c);

double theta;
double phi;

for(int p=0;p<r;p++){
for(int q=p+1;q<c;q++){

theta = atan2(-gsl_matrix_get(A,q,p)+gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q)+gsl_matrix_get(A,p,p));
jacobi_matrix_set( G ,p,  q , A,theta);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,G,A,0.0f,D);

phi = 0.5f*atan(2.0f*gsl_matrix_get(D,p,q)/(gsl_matrix_get(D,q,q)-gsl_matrix_get(D,p,p)));
jacobi_matrix_set( J ,p,  q , D,phi);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,V,J,0.0f,D);
gsl_matrix_memcpy(V,D);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,U,G,0.0f,D);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,D,J,0.0f,U);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,G,A,0.0f,D);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0f,J,D,0.0f,A);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0f,A,J,0.0f,D);
gsl_matrix_memcpy(A,D);
}
}
double toltol=dia_check(tol_v,A);
gsl_matrix_free(J);
gsl_matrix_free(G);
gsl_vector_free(tol_v);
return toltol;

}

// -------------------------------------------------------------------------

// Diagonal tol check ------------------------------------------------------

double dia_check(gsl_vector* tol_v,gsl_matrix *A){
int r =A -> size1;
for(int p=0;p<r;p++){gsl_vector_set(tol_v,p,gsl_vector_get(tol_v,p)-gsl_matrix_get(A,p,p));}
return gsl_blas_dnrm2(tol_v);
}

// -------------------------------------------------------------------------

// Rotator function --------------------------------------------------------

void jacobi_matrix_set(gsl_matrix* Jpq ,int p, int q,gsl_matrix* A,double phi){

int n = A->size1;

for(int i =0;i<n;i++){
for(int j =0;j<n;j++){

if(i==j){gsl_matrix_set(Jpq,i,j,1);}else{gsl_matrix_set(Jpq,i,j,0);}
if (i==p && j==p){gsl_matrix_set(Jpq,i,j,cos(phi));}
if (i==q && j==q){gsl_matrix_set(Jpq,i,j,cos(phi));}
if (i==p && j==q){gsl_matrix_set(Jpq,i,j,sin(phi));}
if (i==q && j==p){gsl_matrix_set(Jpq,i,j,-1*sin(phi));}

}
}


}

// -------------------------------------------------------------------------

// Display function --------------------------------------------------------

void show_matrix_consol(gsl_matrix* A){
int h =A->size1;
int w =A->size2;
printf("\n\n");
for(int i=0;i<h;i++){for(int j=0;j<w;j++){printf("\t %lf ", gsl_matrix_get(A,i,j));}printf("\n\n");}
printf("\n\n");
}

// -------------------------------------------------------------------------

// decomposer --------------------------------------------------------------


void qr_gs_decomp(gsl_matrix* A,gsl_matrix* R){


int r=A->size1;
int c=A->size2;


gsl_vector *temp_a=gsl_vector_alloc(r);
gsl_vector *temp_aj=gsl_vector_alloc(r);
gsl_vector *temp_q=gsl_vector_alloc(r);
double norm_res;
double qaj_dot;

//first loop ortogonalisering
for (int q=0 ;q<c;q++){

//ai pick
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_a,p,gsl_matrix_get(A,p,q));
}
//find norm
gsl_blas_ddot(temp_a, temp_a, &norm_res);
norm_res=sqrt(norm_res);
//set Rqq
gsl_matrix_set(R,q,q,norm_res);
//qi pick
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_q,p,gsl_vector_get(temp_a,p)/norm_res);
gsl_matrix_set(A,p,q,gsl_vector_get(temp_a,p)/norm_res);
}
//du har nu qi og ai
for(int j=q+1;j<c;j++){
//set aj
for (int p=0 ;p<r;p++){
gsl_vector_set(temp_aj,p,gsl_matrix_get(A,p,j));
}
//find norm
gsl_blas_ddot(temp_q, temp_aj, &qaj_dot);
// set Rqj
gsl_matrix_set(R,q,j,qaj_dot);
// set Qpj
for (int p=0 ;p<r;p++){
gsl_matrix_set(A,p,j,gsl_vector_get(temp_aj,p)-qaj_dot*gsl_vector_get(temp_q,p));
}

}


}

gsl_vector_free(temp_a);
gsl_vector_free(temp_aj);
gsl_vector_free(temp_q);


}
// ----------------------------------------------------------------------






