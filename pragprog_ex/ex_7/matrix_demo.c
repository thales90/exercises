#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
int gsl_linalg_HH_solve (gsl_matrix * A, const gsl_vector * b, gsl_vector * x);



int main(){

gsl_matrix* matrix = gsl_matrix_alloc(3,3);
gsl_matrix* matrix_clone = gsl_matrix_alloc(3,3);
gsl_vector* b=gsl_vector_alloc(3);
gsl_vector* x=gsl_vector_alloc(3);
gsl_vector* mul_b=gsl_vector_alloc(3);

gsl_matrix_set(matrix,0,0,6.13);
gsl_matrix_set(matrix,1,0,-2.90);
gsl_matrix_set(matrix,0,1,8.08);
gsl_matrix_set(matrix,1,1,-6.31);
gsl_matrix_set(matrix,1,2,1);
gsl_matrix_set(matrix,2,1,-3.89);
gsl_matrix_set(matrix,2,2,0.19);
gsl_matrix_set(matrix,2,0,5.86);
gsl_matrix_set(matrix,0,2,-4.36); 


gsl_vector_set(b,0, 6.23 ) ;
gsl_vector_set(b,1, 5.37 ) ;
gsl_vector_set(b,2, 2.29 ) ;


gsl_matrix_memcpy(matrix_clone,matrix);




//printf 
int i,j;
printf("the matrix equation  :\n");
for ( j=0;j<3;j++){
printf("[");
for ( i = 0 ; i<3;i++){

printf("%f  ",gsl_matrix_get(matrix,i,j));

}
printf("]");
printf("[x%d]  = [%f] \n",j+1,gsl_vector_get(b,j));

}
printf("\n");
printf("\n solving ... \n");
gsl_linalg_HH_solve (matrix, b, x);


for ( j=0;j<3;j++){

printf(" [x%d] = [%f]  \n",j+1, gsl_vector_get(x,j));

}

printf("\n mutiplying back : \n");

//printf 

gsl_blas_dgemv(CblasNoTrans,1.0f,matrix_clone,x,0.0f,mul_b);
for ( j=0;j<3;j++){

printf(" [mul_b%d] = [%f]  \n",j+1, gsl_vector_get(mul_b,j));

}


return 0;
}



