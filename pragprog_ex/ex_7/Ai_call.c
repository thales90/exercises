#include<stdio.h>
#include<gsl/gsl_sf_airy.h>


int main(){
double x;
while( scanf("%lg",&x) != EOF ) printf("%lg \t %lg\n",x,gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE));
return 0;
}

