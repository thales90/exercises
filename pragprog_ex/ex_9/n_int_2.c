#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>


double norm (double x, void * params) {
  double alpha = *(double *) params;
  double f = exp(-alpha*x*x);
  return f;
}

double sandwish (double x, void * params) {
  double alpha = *(double *) params;
  double f = (-alpha*alpha*x*x*0.5f+alpha*0.5f+x*x*0.5f)*exp(-alpha*x*x);
  return f;
}

int main(){


double alpha;
double result, error;


while( scanf("%lg",&alpha) != EOF )
{
  gsl_integration_workspace * w
    = gsl_integration_workspace_alloc (1000);

  gsl_function F;
  F.function = &norm;
  F.params = &alpha;

  gsl_integration_qagi (&F, 0, 1e-7, 1000,
                        w, &result, &error);




  gsl_integration_workspace_free (w);


double result2, error2;

  gsl_integration_workspace * w2
    = gsl_integration_workspace_alloc (1000);

  gsl_function F2;
  F2.function = &sandwish;
  F2.params = &alpha;

  gsl_integration_qagi (&F2, 0, 1e-7, 1000,
                        w2, &result2, &error2);

  
	printf("%lg \t %lg\n",alpha,result2/result);


}



return 0;
}
