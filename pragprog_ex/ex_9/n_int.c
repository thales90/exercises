#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>


double f (double x, void * params) {
  double alpha = *(double *) params;
  double f = log(alpha*x) / sqrt(x);
  return f;
}

int main(){

printf("Part 1 : \n\n");
printf("integrating ln(x)/sqrt(x) on (0,1) \n\n");
  gsl_integration_workspace * w
    = gsl_integration_workspace_alloc (1000);

  double result, error;
  double expected = -4.0;
  double alpha = 1.0;

  gsl_function F;
  F.function = &f;
  F.params = &alpha;

  gsl_integration_qags (&F, 0, 1, 0, 1e-7, 1000,
                        w, &result, &error);

  printf ("result          = % .18f\n\n", result);


  gsl_integration_workspace_free (w);


printf("Part 2 : \n\n");
printf("DERREGNES  !!!\n\n");

printf(" the reason that the energi gets minimized around a=1 \n");
printf(" is that you give the gausian the least spread , and thus being trap in a harmonic occilator \n");
printf(" the energy cant excede the spreed  \n");

return 0;
}
