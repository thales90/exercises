#include"komplex.h"
#include<stdio.h>

int main(){


komplex a={1,2}, b={3,4};
komplex_print("\na = ",a);
komplex_print("b = ",b);
printf("\n----- testing komplex strukt , _add -----\n");
komplex r=komplex_add(a,b);
komplex R={4,6};


komplex_print("a+b should = ",R);
komplex_print("a+b is = ",r);

printf("\n----- testing komplex strukt , _sub -----\n");
r=komplex_sub(a,b);
komplex S={-2,-2};
komplex_print("a-b should = ",S);
komplex_print("a-b is = ",r);


return 0;
}
