#include"pthread.h" 
#include"math.h"
#include"stdio.h"

void* bar(void* arg){ /* a function to run in a separate thread */
	double *x=(double*)arg;
	for(int i=0;i<1e8;i++) *x=cos(*x); /* do your stuff here */
	return NULL;
}

int main() {
	pthread_t thread;
	double x=0,y=100;
	int flag=pthread_create(&thread,NULL,bar,(void*)&x);

	bar((void*)&y); /* meanwhile in the master thread... */
	flag = pthread_join(thread,NULL); /* join the other thread */
	printf("x=%g\ny=%g\n",x,y);
return 0;
}
