

the greatest = 2147483647 , from lib 


---- please wait for loop ----

the greatest = 2147483647 as gained from while loop !

---- please wait for loop ----

the greatest = 2147483647 as gained from for loop !

---- please wait for loop ----

the greatest = 2147483647 as gained from do_while loop !


the smallest = -2147483648 , from lib 


---- please wait for loop ----

the smallest = -2147483648 as gained from while loop !

---- please wait for loop ----

the smallest = -2147483648 as gained from for loop !

---- please white for loop ----

the smallest = -2147483648 as gained from do_while loop !

FLT EPSILON = 1.1920928955e-07 as from lib
DBL_EPSILON = 2.2204460493e-16 as from lib
LDBL_EPSILON = 1.0842e-19 as from lib 

epsilon_flt = 1.1920928955e-07 as gained from while loop !
epsilon_dbl = 2.2204460493e-16 as gained from while loop !
epsilon_ldbl = 1.0842e-19 as gained from while loop !
epsilon_flt = 1.1920928955e-07 as gained from do while loop !
epsilon_flt = 1.1920928955e-07 as gained from for loop !
epsilon_dbl = 2.2204460493e-16 as gained from do while loop !
epsilon_dbl = 2.2204460493e-16 as gained from for loop !
epsilon_ldbl = 1.0842e-19 as gained from do while loop !
epsilon_ldbl = 1.0842e-19 as gained from for loop !

Sum up / dw , of harmonik series S = 1 + 1/2 + 1/3 .. + 1/max

sum starting at 1 = 15.403683
sum starting at 1/max = 18.807919 

ii) difference is because float has a low decimal presision. 
iii) no its the harmonic series !

If we change it to double type :
sum starting at 1 = 20.678484
sum starting at 1/max = 20.678484 

we see that the difference has become samller as our (TYPE)_EPSILON has gotten smaller.

testing function a=6 , b=8 , tau = 3, epsilon = 3
 
we get equal(6,8,3,3) = 1

