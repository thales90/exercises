#include <limits.h>
#include <float.h>
#include <stdio.h>

void part2(){


printf("FLT EPSILON = %.10e as from lib\nDBL_EPSILON = %.10e as from lib\nLDBL_EPSILON = %Lg as from lib \n\n",FLT_EPSILON,DBL_EPSILON,LDBL_EPSILON);

float x=1;
while(x+1!=1){x/=2;}
x*=2;
printf("epsilon_flt = %.10e as gained from while loop !\n",x);

double dx=1;
while(dx+1!=1){dx/=2;}
dx*=2;
printf("epsilon_dbl = %.10e as gained from while loop !\n",dx);

long double ldx=1;
while(ldx+1!=1){ldx/=2;}
ldx*=2;
printf("epsilon_ldbl = %Lg as gained from while loop !\n",ldx);





x=1;
do{
x/=2;
}while(1+x!=1);
x*=2;
printf("epsilon_flt = %.10e as gained from do while loop !\n",x);


for(x=1;x+1!=1;x/=2){}x*=2;
printf("epsilon_flt = %.10e as gained from for loop !\n",x);


dx=1;
do{
dx/=2;
}while(1+dx!=1);
dx*=2;
printf("epsilon_dbl = %.10e as gained from do while loop !\n",dx);


for(dx=1;dx+1!=1;dx/=2){}dx*=2;
printf("epsilon_dbl = %.10e as gained from for loop !\n",dx);



ldx=1;
do{
ldx/=2;
}while(1+ldx!=1);
ldx*=2;
printf("epsilon_ldbl = %Lg as gained from do while loop !\n",ldx);


for(ldx=1;ldx+1!=1;ldx/=2){}ldx*=2;
printf("epsilon_ldbl = %Lg as gained from for loop !\n\n",ldx);
return;
}







