#include <limits.h>
#include <float.h>
#include <stdio.h>

void part3(){

printf( "Sum up / dw , of harmonik series S = 1 + 1/2 + 1/3 .. + 1/max\n\n" );
int max = INT_MAX/4;
float sum_u = 0,sum_d=0; 
for (int i = 1; i<=max;i++){
sum_u+=1.0f/i;
sum_d+=1.0f/(max-i+1);
}
printf("sum starting at 1 = %f\nsum starting at 1/max = %f \n\n",sum_u,sum_d);
printf("ii) difference is because float has a low decimal presision. \n");
printf("iii) no its the harmonic series !\n\n" ); 


double dmax = INT_MAX/4;
double dsu =0, dsd=0;
for (double i = 1; i<=dmax;i++){
dsu+=1.0/i;
dsd+=1.0/(dmax-i+1);
}
printf("If we change it to double type :\nsum starting at 1 = %lf\nsum starting at 1/max = %lf \n\nwe see that the difference has become samller as our (TYPE)_EPSILON has gotten smaller.",dsu,dsd);




}







