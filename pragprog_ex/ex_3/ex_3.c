
#include <limits.h>
#include <float.h>
#include <stdio.h>


void part1();
void part2();
void part3();
int equal(double a, double b, double tau, double epsilon);

int main(){
part1();
part2();
part3();

printf("\n\ntesting function a=6 , b=8 , tau = 3, epsilon = 3");
printf("\n \nwe get equal(6,8,3,3) = %i\n\n",equal(6,8,3,3)); 

return 0;
}
