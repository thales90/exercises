#include<gsl/gsl_multimin.h>
#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <math.h>

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]);

double
my_f (const gsl_vector *v, void *params)
{
  double A, T,B,r_val=0;
  double *p = (double *)params;

  A = gsl_vector_get(v, 0);
  T = gsl_vector_get(v, 1);
  B = gsl_vector_get(v, 2);

   for (int i =0;i<n;i++){

r_val+=(A*exp(-t[i]/T)+B-y[i])*(A*exp(-t[i]/T)+B-y[i])/(e[i]*e[i]);

	}

  return r_val;
}


int
main(void)
{
 double par[5] = {1.0, 2.0, 10.0, 20.0, 30.0},Ae,Te,Be;

  const gsl_multimin_fminimizer_type *T =
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  /* Starting point */
  x = gsl_vector_alloc (3);
  gsl_vector_set (x, 0, 5.0);
  gsl_vector_set (x, 1, 7.0);
  gsl_vector_set (x, 2, 7.0);
  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (3);
  gsl_vector_set_all (ss, 1.0);

  /* Initialize method and iterate */
  minex_func.n = 3;
  minex_func.f = my_f;
  minex_func.params = par;

  s = gsl_multimin_fminimizer_alloc (T, 3);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status)
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);

      if (status == GSL_SUCCESS)
        {
          //printf ("converged to minimum at\n");
        }
/*
     printf ("%5lu %10.3e %10.3e %10.3e f() = %7.3f size = %.3f\n",
              iter,
              gsl_vector_get (s->x, 0),
              gsl_vector_get (s->x, 1),
	      gsl_vector_get (s->x, 2),
              s->fval, size);
*/

    
        Ae=   gsl_vector_get (s->x, 0);
 	Te=            gsl_vector_get (s->x, 1);
	Be= 	      gsl_vector_get (s->x, 2); 
    }
  while (status == GSL_CONTINUE && iter < 400);

double input;
while( scanf("%lg",&input) != EOF ) printf("%lg \t %lg\n",input,Ae*exp(-input/Te)+Be);




  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return status;
}




