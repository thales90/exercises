#include "math.h"
#include "complex.h"
#include "stdio.h"

double xdg,xdj;
double complex xds,xdei,xdeip,xdie;

int main(){
//printf("\n\n1) Calculate (using C mathematical functions): \n");
xdg = tgamma(5.0L);
xdj = j1l(0.5L);
printf("\n----Function calls , real to real :  ----\n\nThe val of tgamma(5.0L) = %lf\n\nThe val of j1L(0.5L) = %lf\n",xdg,xdj);
xds = csqrt(-2.0f);
xdei = cpowl(M_E,I);
xdeip = cpowl(M_E,I*M_PI);
xdie = cpowl(I,M_E);
printf("\n\n----Now we look at the complex calls:  ----\n\nM_E = math.h macro for eulers konstant.\n\nThe sqrt(-2) [ csqrt(-2.0f) ] = %Lf + i*%Lf\n\nexp(i) [ cpowl(M_E,I) ] = %Lf+i*%Lf\n\nexp(i*pi) [ cpowl(M_E,I*PI) ] = %Lf+i*%Lf\n\npow(i,e) [ cpowl(I,M_E) ] =%Lf+i*%Lf\n\n",creall(xds),cimagl(xds),creall(xdei),cimagl(xdei),creall(xdeip),cimagl(xdeip),creall(xdie),cimagl(xdie));



return 0;
}
