#include<math.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc,char *argv[]){
double a = atoi(argv[1]);
double b = atoi(argv[2]);
double dx = atof(argv[3]);
double xi;
int N=round((float)(b-a)/dx);
for(int i=0;i<(N+round(10*1/dx));i++)
{
xi=i*(b-a)/N+a;

printf("%lf \t %lf \t %lf \n",xi,sqrt(pow(xi,2)+1),-sqrt(pow(xi,2)+1));
}

return 0;
}
