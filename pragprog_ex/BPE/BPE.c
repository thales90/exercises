#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_eigen.h>
void matrix_set(gsl_matrix* A,double x){

gsl_matrix_set(A,0,0,1.0f);
gsl_matrix_set(A,1,1,-1.0f);
gsl_matrix_set(A,1,0,x);
gsl_matrix_set(A,0,1,x);

}
int main(int argc,char *argv[] ){

double a = atoi(argv[1]);
double b = atoi(argv[2]);
double dx = atof(argv[3]);
double xi;
gsl_matrix *A=gsl_matrix_alloc(2,2);
gsl_vector *v=gsl_vector_alloc(2);
gsl_eigen_symm_workspace *w=gsl_eigen_symm_alloc(2);
int N=round((float)(b-a)/dx);

for(int i=0;i<(N+round(10*1/dx));i++)
{
xi=i*(b-a)/N+a;
matrix_set(A,xi);
gsl_eigen_symm(A,v,w);
printf("%lf \t %lf \t %lf \n",xi,gsl_vector_get(v,0) ,gsl_vector_get(v,1));
}
gsl_matrix_free(A);
gsl_vector_free(v);
gsl_eigen_symm_free(w);
return 0;
}
