#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include<gsl/gsl_odeiv2.h>
#include <gsl/gsl_matrix.h>
#include<math.h>






int func (double t, const double y[], double f[],void *params)
{
  double ep = *(double *)params;
  f[0] = y[1];
  f[1] = -2*(ep+1/t)*y[0];
  return GSL_SUCCESS;
}


double
difi (double x, void *params)
{
 

   double return_val=0;
   
   gsl_odeiv2_system sys = {func, NULL, 2, &x};

    gsl_odeiv2_driver * d =gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

  int i;
  double t = 0.001f, t1 = 8;
  double y[2] = { t-t*t, 1-2*t };

  for (i = 1; i <= 100; i++)
    {
      double ti = i * t1 / 100.0;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

      if (status != GSL_SUCCESS)
        {
          printf ("error, return value=%d\n", status);
          break;
        }

     // printf ("%.5e %.5e %.5e\n", t, y[0], y[1]);
    }

  return_val=y[0];

  //printf ("ODE CALL %.5e %.5e %.5e\n", t, y[0], y[1]);
  gsl_odeiv2_driver_free (d);
  return return_val;

  
}


int
main (void)
{


  int status;
  int iter = 0, max_iter = 100;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;
  double r = 0, r_expected = -0.5f;
  double x_lo = -1.1f, x_hi =-0.2f;
  gsl_function F;
  double params =10;
 

  F.function = &difi;
  F.params = &params;

  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_lo, x_hi);
/*
  printf ("using %s method\n",
          gsl_root_fsolver_name (s));

  printf ("%5s [%9s, %9s] %9s %10s %9s\n",
          "iter", "lower", "upper", "root",
          "err", "err(est)");
*/

  do
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      r = gsl_root_fsolver_root (s);
      x_lo = gsl_root_fsolver_x_lower (s);
      x_hi = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_lo, x_hi,
                                       0, 0.001);
       /*
      if (status == GSL_SUCCESS)
        printf ("Converged:\n");

      printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
              iter, x_lo, x_hi,
              r, r - r_expected,
              x_hi - x_lo);
	*/
    }
  while (status == GSL_CONTINUE && iter < max_iter);





    params =r;
  
   
    gsl_odeiv2_system sys = {func, NULL, 2, &params};

    gsl_odeiv2_driver * d =gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

  int i;
  double t = 0.001f, t1 = 8;
  double y[2] = { t-t*t, 1-2*t };
  
  for (i = 1; i <= 100; i++)
    {
      double ti = i * t1 / 100.0;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

      if (status != GSL_SUCCESS)
        {
          printf ("error, return value=%d\n", status);
          break;
        }

      printf ("%.5e \t %.5e\n", t, y[0]);
    }


 
  gsl_odeiv2_driver_free (d);






  gsl_root_fsolver_free (s);




return 0;
}

