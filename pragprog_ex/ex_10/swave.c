#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include<gsl/gsl_odeiv2.h>
#include <gsl/gsl_matrix.h>
#include<math.h>






int func (double t, const double y[], double f[],void *params)
{
  double ep = *(double *)params;
  f[0] = y[1];
  f[1] = -2*(ep+1/t)*y[0];
  return GSL_SUCCESS;
}


int
main (void)
{

   double params =-0.5f;
   double return_val=0;
   
   gsl_odeiv2_system sys = {func, NULL, 2, &params};

    gsl_odeiv2_driver * d =gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

  int i;
  double t = 0.001f, t1 = 8;
  double y[2] = { t*exp(-1*t), exp(-t) -t*exp(-t) };
  
  for (i = 1; i <= 100; i++)
    {
      double ti = i * t1 / 100.0;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

      if (status != GSL_SUCCESS)
        {
          printf ("error, return value=%d\n", status);
          break;
        }

      printf ("%.5e \t %.5e\n", t, y[0]);
    }


 
  gsl_odeiv2_driver_free (d);


  

return 0;
}

