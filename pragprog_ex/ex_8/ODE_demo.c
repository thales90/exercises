#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int func (double t, const double y[], double f[],void *params)
{
  (void)(t);
  f[0] = y[0]*(1-y[0]);
 
  return GSL_SUCCESS;
}

int *jac ;

int main(){


int dim =1;
gsl_odeiv2_system sys = {func,NULL,dim,NULL};
gsl_odeiv2_driver *d=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,1e-6,1e-6,0.0);
int i;
double x0=0.0f,xf=3.0f;
double x =x0;
double xi;
double y[1]={0.5f};
float N=100.0f; //presision

while( scanf("%lg",&xi) != EOF )
{


int status= gsl_odeiv2_driver_apply(d,&x,xi,y);
if(status!=GSL_SUCCESS){
printf("\n ERRRORORORORRORO!!! \n ");
break;
}
printf("%lg \t %lg\n",x,y[0]);

}


gsl_odeiv2_driver_free(d);
return 0 ; 
} 
