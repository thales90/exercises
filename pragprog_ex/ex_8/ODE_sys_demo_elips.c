#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include<math.h>

int func (double t, const double y[], double f[],void *params)
{
  (void)(t);
  double ep = *(double *)params;
  f[0] = y[1];
  f[1] = 1-y[0]+ep*y[0]*y[0];
  return GSL_SUCCESS;
}

int
*jac;
/*
(double t, const double y[], double *dfdy,
     double dfdt[], void *params)
{
  (void)(t); 
  double ep = *(double *)params;
  gsl_matrix_view dfdy_mat= gsl_matrix_view_array (dfdy, 2, 2);
  gsl_matrix * m = &dfdy_mat.matrix;
  gsl_matrix_set (m, 0, 0, 0.0);
  gsl_matrix_set (m, 0, 1, 1.0);
  gsl_matrix_set (m, 1, 0, +2.0f*ep*y[0] - 1.0);
  gsl_matrix_set (m, 1, 1, 0);
  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  return GSL_SUCCESS;
}
*/
int main(){
double ep=0;
int dim =2;
gsl_odeiv2_system sys = {func,NULL,dim,&ep};
gsl_odeiv2_driver *d=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rk8pd,1e-6,1e-6,0.0);
double x =0;
double xi;
double y[2]={1.0f,-0.5f};

while( scanf("%lg",&xi) != EOF )
{


int status= gsl_odeiv2_driver_apply(d,&x,xi,y);
if(status!=GSL_SUCCESS){
printf("\n ERRRORORORORRORO!!! \n ");
break;
}
printf("%lg \t %lg\n",1.0f/y[0]*cos(x),1.0f/y[0]*sin(x));

}


gsl_odeiv2_driver_free(d);
return 0 ; 
} 
