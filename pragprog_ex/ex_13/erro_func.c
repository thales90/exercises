#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int func (double t, const double y[], double f[],void *params)
{
  (void)y[0];
  f[0] = (2.0f/sqrt(M_PI))*exp(-t*t);
 
  return GSL_SUCCESS;
}


int main(int argc , char *argv[]){

double a = atoi(argv[1]);
double b = atoi(argv[2]);
double dx = atof(argv[3]);

int dim =1;
gsl_odeiv2_system sys = {func,NULL,dim,NULL};
gsl_odeiv2_driver *d=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,1e-6,1e-6,0.0);

double x =a;
double xi;
double y[1]={0};

int N=round((float)(b-a)/dx);

for(int i=0;i<(N+round(10*1/dx));i++)
{

xi=i*(b-a)/N+a;

int status= gsl_odeiv2_driver_apply(d,&x,xi,y);
if(status!=GSL_SUCCESS){
printf("\n ERRRORORORORRORO!!! \n ");
break;
}
printf("%lg \t %lg\n",x,y[0]);

}


gsl_odeiv2_driver_free(d);
return 0 ; 

}
